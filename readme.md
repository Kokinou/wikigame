# wikigame
***
But du projet : un programme permettant d'extraire les URLS d'un page de wikipedia.
On part d'une page aléatoire, on doit arriver à une page aléatoire.
Le joueur doit arriver a la page d'arrivée uniquement grâce aux URLS extraites des différentes page.

***
## Table des matières
1. [Info-General](#info-general)
2. [Technologies](#technologies)
3. [Installation](#installation)

***
##Info-General

***
Les URLs sont prise uniquement dans la partie centrale de la page wikipedia.
Pas d'URLs en double.
Pas de liens du sommaire.

***
##Technologies

***
* [Python](https://www.python.org/) Version 3.9
* [Pip](https://pypi.org/project/pip/) Version 21.0.1
* [beautifulsoup4](https://pypi.org/project/beautifulsoup4/) Version 4.9.3
* [os-sys](https://pypi.org/project/os-sys/) Version 2.1.4
* [urllib](https://pypi.org/project/urllib3/) Version 1.26.3

***
##Installation

***
Pour récupérer le programme :

$ git clone https://gitlab.com/Kokinou/wikigame.git

***
Pour le lancer, il faudra au préalable installer pip et les librairies: 
Télécharger get-pip.py 
Executez le dans l'invite de commande : python get-pip.py

***
Ensuite vous pourez installer les différentes libraires listées plus haut :
pip install bs4
pip3 install urllib3
pip install os-sys

***
Pour lancer le programme, il vous suffira de vous placer dans le dossier contenant le fichier wikigame.py
Puis faire : Python wikigame.py