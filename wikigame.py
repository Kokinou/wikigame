from bs4 import BeautifulSoup #permet de creer un objet Python representant un document HTML.

import urllib.request #module permettant d'ouvrir les URLs.
import os # module permettant d'utiliser les fonctionnalités dépendantes du système d’exploitation.
import urllib.parse # permet l'utilisation de unquote pour décoder les URLs
import sys



# -*- coding: utf-8 -*-





#page_de_depart = " https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Page_au_hasard "  # la page random sur laquelle le joeur débute
page_de_depart = " https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Page_au_hasard "
page_d_arrivee = " https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Page_au_hasard " # la page qu'il doit atteindre
page_actuelle = " " 
liste_url = [] 	# on va utiliser une liste pour stocker les urls
liste_url_tri = []	# on va utiliser supprimer les doublons 
liste_url_aff = []  # la liste qui sera affichée avec des caractères lisibles

compteur = 0 # stocke le nombre de tours effectués par le joueur

print ("\n BUT DU JEU : tirer 2 pages Wikipédia au sort, et d’aller de la 1er à la 2eme, uniquement en utilisant les liens hypertexte présent sur la page, le plus vite possible\n\n")
with urllib.request.urlopen(page_d_arrivee) as response: #ouverture de la page web
	webpage = response.read() #lecture de celle-ci
	soup = BeautifulSoup(webpage, 'html.parser') #ici onparse le HTML
	titre = soup.find("h1", {"class" : "firstHeading"}) #recuperation tu titre h1 de la pageweb
	arrivee = titre.text
	print("VOUS DEVEZ ATTEINDRE LA PAGE DE : {}\n".format(arrivee))
	




## ici on va parcourir tous les DIV UL LI A pour retrouver tous les liens de la page web de départ ##
with urllib.request.urlopen(page_de_depart) as response:
	webpage = response.read()
	soup = BeautifulSoup(webpage, 'html.parser')
	titre_depart = soup.find("h1", {"class" : "firstHeading"})
	depart = titre_depart.text
	print("page de départ : {}\n".format(depart))

	for div in soup.find_all("div", {"class" : "mw-parser-output"}):
		for p in div.findChildren('p'):
			for a in p.findChildren('a'):
					tmp = a.get('href')					#On a récuperé tous les URLs
					if not tmp.find(":") != -1:			#On ne conserve pas celles qui contiennent : 
						if not tmp.find("#") != -1:		#On ne conserve pas celles qui contiennent un #
							if tmp.find("wiki") != -1:
								
								liste_url.append(tmp)		#on les ajoute à la première liste


## ici on va trier la première liste, transfert vers la seconde pour supprimer les doublons, transfert vers une troisième avec les bons caractères ##


										#tri de la liste

liste_url.sort()
for urls in liste_url:
	if urls not in liste_url_tri:
		liste_url_tri.append(urls) #on enlève les doublons
		
liste_url_tri.sort()
for urls in liste_url_tri:
	if urls not in liste_url_aff:
		liste_url_aff.append(urllib.parse.unquote_plus(urls)) #on l'ajoute en decodans l'URL pour avoir un affichage "propre" de tous les caractères.

liste_url_aff.sort()
for url in liste_url_aff:		# parcours et affichage de la liste des Urls
	print("https://fr.wikipedia.org{}\n".format(url))
			

## ici on va mettre en place une boucle qui nous servivra pour mettre en place le jeu ##

while (page_actuelle != page_d_arrivee):

	print("VOUS DEVEZ ATTEINDRE LA PAGE DE : {}\n".format(arrivee))

	page = input("quel sera votre prochain choix ?")
	os.system("cls")									#on clean la console
	liste_url[:] = []
	liste_url_tri[:] = []									#on vide les trois listes
	liste_url_aff[:] = []

	page_actuelle = urllib.parse.quote(page,safe=':/') # permet de ne pas avoir de problèmes d'encodage.

	with urllib.request.urlopen(page_actuelle) as response:
		webpage = response.read()
		soup = BeautifulSoup(webpage, 'html.parser')

		for div in soup.find_all("div", {"class" : "mw-parser-output"}):
			for p in div.findChildren('p'):
				for a in p.findChildren('a'):
					tmp = a.get('href')
					if not tmp.find(":") != -1:
						if not tmp.find("#") != -1:
							if tmp.find("wiki") != -1:
								
								liste_url.append(tmp)
							
						
	liste_url.sort()
	for urls in liste_url:
		if urls not in liste_url_tri:
			liste_url_tri.append(urls) #on enlève les doublons
		
	liste_url_tri.sort()
	for urls in liste_url_tri:
		if urls not in liste_url_aff:
			liste_url_aff.append(urllib.parse.unquote_plus(urls)) #on l'ajoute en decodans l'URL pour avoir un affichage "propre" de tous les caractères.

	liste_url_aff.sort()
	for url in liste_url_aff:		# parcours et affichage de la liste des Urls
		print("https://fr.wikipedia.org{}\n".format(url))
											
	compteur = compteur + 1					#nombre de tours
	print ("NOMBRE DE TOURS EFFECTUES : {}".format(compteur))

print("Vous avez reussie en {} coups ".format (compteur))